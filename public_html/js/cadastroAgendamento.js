//função em que vai monitorar o cadastramento de agendamento
$(document).ready(function () {
    $('#form').submit(function () {
        var idSala = $('#idSala').val();
        var dataE = $('#dataE').val();
        var horaInicio = $('#horaInicio').val() + ":" + $('#minutosInicio').val();
        var horaTermino = $('#horaTermino').val() + ":" + $('#minutosTermino').val();
        // var atividade = $('#atividade').val();
        var para = $('#para').val();
        $.ajax({//Função AJAX
            url: "realizarAgendamento",
            type: "post",
            data: {idSala: idSala, dataE: dataE, horaInicio: horaInicio, horaTermino: horaTermino,para: para},
            success: function (result) {
                if (result == 0) {
                    $("#sucesso").modal();

                } else {
                    if (result == 10) {
                        $("#avisoHoraNao").hide();

                        $("#avisoHoraMaior").show();
                        $("#avisoHoraMaior").removeClass(' alert alert-success');
                        $("#avisoHoraMaior").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! A hora de início não pode ser maior que a hora de término!</center>");
                    }
                    if (result == 20) {
                        $("#avisoHoraMaior").hide();
                        $("#avisoHoraNao").show();
                        $("#avisoHoraNao").removeClass(' alert alert-success');
                        $("#avisoHoraNao").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! A horário desejado não está disponível!</center>");
                    }

                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;
    });
});
