//função em que vai monitorar o cadastramento de agendamento
$(document).ready(function () {
    $('#form').submit(function () {
        var codigo = $('#codigoPalete').val();
        $.ajax({//Função AJAX
            url: "verificaPaleteOrigem",
            type: "post",
            data: {codigo: codigo},
            success: function (result) {
                if (result == 0) {
                    //location.href = "/sga/public_html/apontarPalete/" + codigo;
                    urll = 'apontarGenerioMatrial';
                    //Define o formulário
                    var myForm = document.createElement("form");
                    myForm.action = urll;
                    myForm.method = "post";

                    var input = document.createElement("input");
                    input.type = "text";
                    input.value = codigo;
                    input.name = 'codigo';
                    myForm.appendChild(input);
                    //Adiciona o form ao corpo do documento
                    document.body.appendChild(myForm);
                    //Envia o formulário
                    myForm.submit();

                } else {
                    if (result == 1) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Código já utilizado. Verifique o status do código!</center>");
                    }
                    if (result == 2) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Palete não apontado!</center>");
                    }
                    if (result == 2) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Palete inexistente ou não endereçado!</center>");
                    }
                    if (result == 22) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Palete já expedido!</center>");
                    }
                    if (result == 10) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! O material do palete deve ser em saco para realizar a operação!</center>");
                    }
                    if (result == 11) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Código inválido!</center>");
                    }
                    if (result == 12) {
                        //REDIRECIONANDO PARA A PÁGINA DE PALETE GENÉRICO PARA PALETE GENÉRICO
                        //location.href = "/sga/public_html/apontarPalete/" + codigo;
                        urll = 'apontarGenericoGenerico';
                        //Define o formulário
                        var myForm = document.createElement("form");
                        myForm.action = urll;
                        myForm.method = "post";

                        var input = document.createElement("input");
                        input.type = "text";
                        input.value = codigo;
                        input.name = 'codigo';
                        myForm.appendChild(input);
                        //Adiciona o form ao corpo do documento
                        document.body.appendChild(myForm);
                        //Envia o formulário
                        myForm.submit();
                    }
                    if (result == 13) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! O palete genérico não contém material!</center>");
                    }

                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;


    });
});
