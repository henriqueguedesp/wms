
$(document).ready(function () {

    $('#tipoE').change(function () {

        var es = document.getElementById('tipoE');
        var saldo = document.getElementById('saldo');

        esValor = es.options[es.selectedIndex].value;
        if (esValor == '-1') {
            document.getElementById("quantidade").max = -1;
            document.getElementById("itemSaldo").value = "Selecione um item!";
        } else {
            valor = saldo.options[es.selectedIndex - 1].value;

            document.getElementById("quantidade").max = valor;
            document.getElementById("itemSaldo").value = valor;
        }
    });

});

$(document).ready(function () {
    $('#form').submit(function () {
        var destino = $('#codigoDestino').val();
        var quantidade = $('#quantidade').val();
        var es = document.getElementById('tipoE');
        var origem = es.options[es.selectedIndex].value;
        var palete = $('#codigoOrigem').val();
        $.ajax({//Função AJAX
            url: "verificaPaleteDestinoGenerico",
            type: "post",
            data: {origem: origem, destino: destino, quantidade: quantidade, palete: palete},
            success: function (result) {
                if (result == 1) {
                    alert("Apontamento realizado com sucesso!");
                    location.href = "/sga/public_html/";


                } else {
                    if (result == 10) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! O código digitado não corresponde a um palete genérico!</center>");
                    }
                    if (result == 11) {
                        $("#avisoItem").hide();
                        $("#avisoItem").show();
                        $("#avisoItem").removeClass(' alert alert-success');
                        $("#avisoItem").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Selecione um item!</center>");
                    }
                    if (result == 12) {
                        $("#avisoQuantidade").hide();
                        $("#avisoQuantidade").show();
                        $("#avisoQuantidade").removeClass(' alert alert-success');
                        $("#avisoQuantidade").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Digite a quantidade!</center>");
                    }
//                    alert(result);
                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;

    });
});

 