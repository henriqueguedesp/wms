//função em que vai monitorar o cadastramento de agendamento
$(document).ready(function () {
    $('#form').submit(function () {
        var codigo = $('#codigoPalete').val();
        $.ajax({//Função AJAX
            url: "verificaCodigo",
            type: "post",
            data: {codigo: codigo},
            success: function (result) {
                if (result == 0) {
                    //location.href = "/sga/public_html/apontarPalete/" + codigo;
                    document.getElementById('codigoPaleteSalvar').value = codigo;

//                    /$('#botaoEnviarSalvamento').on(click);
                    document.getElementById("botaoEnviarSalvamento").click();


                } else {
                    if (result == 1) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Código já utilizado. Verifique o status do código!</center>");
                    }
                    if (result == 2) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Campo do código está vazio!</center>");
                    }
                    if (result == 3) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! O código lido está incorreto!</center>");
                    }

                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;


    });
});
