//função em que vai monitorar o cadastramento de agendamento
$(document).ready(function () {
    $('#form').submit(function () {
        var codigo = $('#codigoPalete').val();
        var id = $('#id').val();
        var quantidade = $('#quantidade').val();
        $.ajax({//Função AJAX
            url: "salvarReapontar",
            type: "post",
            data: {codigo: codigo,id:id,quantidade:quantidade},
            success: function (result) {
                if (result == 0) {
                    jQuery.noConflict();
                        $("#sucesso").modal();

                } else {
                    if (result == 1) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Código já utilizado. Por favor verifique!</center>");
                    }
                    if (result == 2) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Campo do código está vazio!</center>");
                    }
                    if (result == 3) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! O código lido está incorreto!</center>");
                    }

                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;


    });
});
