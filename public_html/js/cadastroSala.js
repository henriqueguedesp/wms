//função para abri modal de cadastro de sala
function adicionar() {
    $("#cadastrarSala").modal();
    $("#avisoSala").hide();
    document.getElementById('salaC').style.border = "";
    document.getElementById('setorC').style.border = "";
    document.getElementById('salaC').value = "";
    document.getElementById('setorC').value = "";
}

//função em que vai monitorar o cadastramento de sala
$(document).ready(function () {
    $('#form').submit(function () {
        var sala = $('#salaC').val();
        var setor = $('#setorC').val();
        $.ajax({//Função AJAX
            url: "cadastrarSala",
            type: "post",
            data: {sala: sala, setor: setor},
            success: function (result) {
                if (result == 0) {
                    $("#cadastrarSala").modal().hide();
                    $("#sucesso").modal();

                } else {
                    if (result == 1) {
                        $("#avisoSala").show();
                        $("#avisoSala").removeClass(' alert alert-success');
                        $("#avisoSala").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Desecrição da sala já utilizada em no setor!</center>");
                        document.getElementById('salaC').style.border = "2px solid #FF6347";
                        document.getElementById('setorC').style.border = "2px solid #FF6347";
                    }

                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;
    });
});


//função para abri modal de edição de sala
function editar(sala, setor, id) {
    $("#editarSala").modal();
    $("#avisoSalaE").hide();
    document.getElementById('salaE').style.border = "";
    document.getElementById('setorE').style.border = "";
    document.getElementById('salaE').value = sala;
    document.getElementById('setorE').value = setor;
    document.getElementById('idE').value = id;

}

//função para monitar a edição dos dados da sala
$(document).ready(function () {
    $('#formEditar').submit(function () {
        var sala = $('#salaE').val();
        var setor = $('#setorE').val();
        var id = $('#idE').val();
        $.ajax({//Função AJAX
            url: "/sas/public_html/editarSala/" + id + "",
            type: "post",
            data: {sala: sala, setor: setor},
            success: function (result) {
                if (result == 0) {
                    $("#editarSala").modal().hide();
                    $("#sucessoEditar").modal();

                } else {
                    if (result == 1) {
                        $("#avisoSalaE").show();
                        $("#avisoSalaE").removeClass(' alert alert-success');
                        $("#avisoSalaE").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Desecrição da sala já utilizada em no setor!</center>");
                        document.getElementById('salaE').style.border = "2px solid #FF6347";
                        document.getElementById('setorE').style.border = "2px solid #FF6347";
                    }

                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;
    });
});

//função para desativar sala
function confirmacao(id) {
    var resposta = confirm("Deseja desativar a sala?");
    if (resposta == true) {

        window.location.assign("/sas/public_html/desativarSala/" + id);
    }
}

//função para ativar sala
function confirmacaoAtivar(id) {
    var resposta = confirm("Deseja ativar a sala?");
    if (resposta == true) {

        window.location.assign("/sas/public_html/ativarSala/" + id);
    }
}


        