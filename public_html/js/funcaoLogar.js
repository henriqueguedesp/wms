$(document).ready(function () {
    $('.login-form').submit(function () {
        var login = $('#email').val();
        var pass = $('#senha').val();
        $.ajax({//Função AJAX
            url: "validaLogin",
            type: "post",
            data: {usuario: login, senha: pass},

            success: function (result) {
                if (result == 1) {
                    location.href = "/sga/public_html/";
                } else if (result == 2) {
                    $("#output").removeClass(' alert alert-success');
                    $("#output").addClass("alert alert-warning animated fadeInUp").html("Usuário está desativado!");
                        document.getElementById('email').style.border = "";
                    document.getElementById('senha').style.border = "";
                

                } else if (result == 0) {

                    //$('#aviso').html('Usuário ou senha errados!');
                    $("#output").removeClass(' alert alert-success');
                    $("#output").addClass("alert alert-danger animated fadeInUp").html("Usuário ou senha está incorreto!");
                    document.getElementById('email').style.border = "2px solid #FF6347";
                    document.getElementById('senha').style.border = "2px solid #FF6347";
                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;
    });
});
