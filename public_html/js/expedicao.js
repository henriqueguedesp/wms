//função em que vai monitorar o cadastramento de agendamento
$(document).ready(function () {
    $('#form').submit(function () {
        var codigo = $('#codigoOrdem').val();
        $.ajax({//Função AJAX
            url: "verificaCodigoOrdem",
            type: "post",
            data: {codigo: codigo},
            success: function (result) {
                if (result == 0) {
                    //location.href = "/sga/public_html/expedirPalete/" + codigo;

                    urll = 'expedirPalete';
                    //obj = '{codigo: 100}';
                    //Define o formulário
                    var myForm = document.createElement("form");
                    myForm.action = urll;
                    myForm.method = "post";
                   
                        var input = document.createElement("input");
                        input.type = "text";
                        input.value = codigo;
                        input.name = 'codigo';
                        myForm.appendChild(input);
                    //Adiciona o form ao corpo do documento
                    document.body.appendChild(myForm);
                    //Envia o formulário
                    myForm.submit();




                } else {
                    if (result == 1) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Ordem já expedida!</center>");
                    }
                    if (result == 3) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! O código da Ordem de Expedição é inválido!</center>");
                    }

                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;
    });
});
