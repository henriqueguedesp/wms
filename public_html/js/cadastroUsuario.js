//função para abri modal de cadastro de sala
function adicionar() {
    jQuery.noConflict();
    $("#cadastrarUsuario").modal();

}
function editar(nome, email, funcao, usuario, senha, id, tipo) {

    $("#editarUsuario").modal();

    document.getElementById('nomeE').value = nome;
    document.getElementById('funcaoE').value = funcao;
    document.getElementById('emailE').value = email;
    document.getElementById('usuarioE').value = usuario;
    document.getElementById('senhaE').value = senha;
    document.formEditar.action = "/sga/public_html/editarUsuario/" + id + "";

//  document.getElementById('id').value = id;

    if (tipo === 0) {

        var comboCidades = document.getElementById("tipoE");
        comboCidades.selectedIndex = 1;

    } else {
        var comboCidades = document.getElementById("tipoE");
        comboCidades.selectedIndex = 0;
    }
}


function confirmacao(id) {
    var resposta = confirm("Deseja desativar o usuário?");
    if (resposta == true) {

        window.location.assign("/sga/public_html/desativarUsuario/" + id);
    }
}
function confirmacaoAtivar(id) {
    var resposta = confirm("Deseja ativar o usuário?");
    if (resposta == true) {

        window.location.assign("/sga/public_html/ativarUsuario/" + id);
    }
}