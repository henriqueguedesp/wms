//função em que vai monitorar o cadastramento de agendamento
$(document).ready(function () {
    $('#form').submit(function () {
        var codigo = $('#novoCodigoPalete').val();
        var id = $('#id').val();
        $.ajax({//Função AJAX
            url: "verificaCodigoReapontar",
            type: "post",
            data: {codigo: codigo,id:id},
            success: function (result) {
                if (result == 0) {
                    //   location.href = "/sga/public_html/apontarPalete/" + codigo;
                    urll = 'reapontarFinal';
                    //Define o formulário
                    var myForm = document.createElement("form");
                    myForm.action = urll;
                    myForm.method = "post";

                    var input = document.createElement("input");
                    input.type = "text";
                    input.value = codigo;
                    input.name = 'codigo';
                    myForm.appendChild(input);
                    var inputo = document.createElement("input");
                    inputo.type = "text";
                    inputo.value = id;
                    inputo.name = 'id';
                    myForm.appendChild(inputo);
                    //Adiciona o form ao corpo do documento
                    document.body.appendChild(myForm);
                    //Envia o formulário
                    myForm.submit();

                } else {
                    if (result == 1) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Código já utilizado. Por favor verifique!</center>");
                    }
                    if (result == 2) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Campo do código está vazio!</center>");
                    }
                    if (result == 3) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! O código lido está incorreto!</center>");
                    }
                    if (result == -2) {
                         jQuery.noConflict();
                        $("#sucesso").modal();
                    }

                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;


    });
});
