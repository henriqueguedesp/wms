//função em que vai monitorar o cadastramento de agendamento
$(document).ready(function () {
    $('#formEditar').submit(function () {
        var ordem = $('#codigoOrdemFinalizar').val();
        var peso = $('#pesoOrdem').val();
        var nota = $('#nota').val();
        $.ajax({//Função AJAX
            url: "finalizarOrdem",
            type: "post",
            data: {codigoOrdemFinalizar: ordem, pesoOrdem: peso, nota: nota},
            success: function (result) {
                if (result == 0) {
                    $('#modalFinalizarOrdem').hide();
                    $('#sucessoFinalizar').modal();

                } else {
                    if (result == 22) {
                        $("#avisoPeso").show();
                        $("#avisoPeso").removeClass(' alert alert-success');
                        $("#avisoPeso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Campo peso vazio!</center>");
                        document.getElementById('pesoOrdem').style.border = "2px solid #FF6347";

                        $("#avisoNota").show();
                        $("#avisoNota").removeClass(' alert alert-success');
                        $("#avisoNota").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Campo nota vazio!!</center>");
                        document.getElementById('nota').style.border = "2px solid #FF6347";

                    }
                    if (result == 23) {
                        $("#avisoNota").hide();
                        document.getElementById('nota').style.border = "";

                        $("#avisoPeso").show();
                        $("#avisoPeso").removeClass(' alert alert-success');
                        $("#avisoPeso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Campo do código está vazio!</center>");
                        document.getElementById('pesoOrdem').style.border = "2px solid #FF6347";

                    }
                    if (result == 24) {
                        $("#avisoPeso").hide();
                        document.getElementById('pesoOrdem').style.border = "";

                        $("#avisoNota").show();
                        $("#avisoNota").removeClass(' alert alert-success');
                        $("#avisoNota").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Campo do código está vazio!</center>");
                        document.getElementById('nota').style.border = "2px solid #FF6347";
                    }

                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;


    });
});
