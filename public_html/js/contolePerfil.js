$(document).ready(function () {
    $('#form').submit(function () {
        var nome = $('#nomeE').val();
        var usuario = $('#usuarioE').val();
        var email = $('#emailE').val();
        var funcao = $('#funcaoE').val();
        var senha = $('#senhaE').val();
        var id = $('#idE').val();
        $.ajax({//Função AJAX
            url: "atualizarPerfil",
            type: "post",
            data: {nome: nome, usuario: usuario, email: email, funcao: funcao, senha: senha, id: id},
            success: function (result) {
                if (result == 0) {
                    $("#sucesso").modal();

                } else {
                    if (result == 1) {
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Já tem perfil com o usuário digitado!</center>");
                               document.getElementById('usuarioE').style.border = "2px solid #FF6347";
                    }

                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;
    });
});