<?php

namespace UBSValorem\Entity;

class Apontamento {

    private $idApontamento;
    private $codigoPalete;
    private $tipo;
    private $saldo;
    private $status;

    function __construct() {
        
    }

    function getIdApontamento() {
        return $this->idApontamento;
    }

    function getCodigoPalete() {
        return $this->codigoPalete;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getSaldo() {
        return $this->saldo;
    }

    function getStatus() {
        return $this->status;
    }

    function setIdApontamento($idApontamento) {
        $this->idApontamento = $idApontamento;
    }

    function setCodigoPalete($codigoPalete) {
        $this->codigoPalete = $codigoPalete;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setSaldo($saldo) {
        $this->saldo = $saldo;
    }

    function setStatus($status) {
        $this->status = $status;
    }

}
