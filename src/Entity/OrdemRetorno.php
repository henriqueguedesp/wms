<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace UBSValorem\Entity;

/**
 * Description of OrdemRetorno
 *
 * @author henrique.guedes
 */
class OrdemRetorno {
    private $idOrdemRetorno;
    private $idUsuario;
    private $codigoRetorno;
    private $codigoLote;
    private $status;
    private $data;
    function __construct() {
        
    }

    function getIdOrdemRetorno() {
        return $this->idOrdemRetorno;
    }

    function getIdUsuario() {
        return $this->idUsuario;
    }

    function getCodigoRetorno() {
        return $this->codigoRetorno;
    }

    function getCodigoLote() {
        return $this->codigoLote;
    }

    function getStatus() {
        return $this->status;
    }

    function getData() {
        return $this->data;
    }

    function setIdOrdemRetorno($idOrdemRetorno) {
        $this->idOrdemRetorno = $idOrdemRetorno;
    }

    function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }

    function setCodigoRetorno($codigoRetorno) {
        $this->codigoRetorno = $codigoRetorno;
    }

    function setCodigoLote($codigoLote) {
        $this->codigoLote = $codigoLote;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setData($data) {
        $this->data = $data;
    }


}
