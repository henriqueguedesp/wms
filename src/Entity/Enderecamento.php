<?php

namespace UBSValorem\Entity;

class Enderecamento {

    private $idEnderecamento;
    private $idApontamento;
    private $codigoPosicao;
    private $status;
    private $dataEnderecamento;
    
    function __construct() {
        
    }

    
    function getIdEnderecamento() {
        return $this->idEnderecamento;
    }

    function getIdApontamento() {
        return $this->idApontamento;
    }

    function getCodigoPosicao() {
        return $this->codigoPosicao;
    }

    function getStatus() {
        return $this->status;
    }

    function getDataEnderecamento() {
        return $this->dataEnderecamento;
    }

    function setIdEnderecamento($idEnderecamento) {
        $this->idEnderecamento = $idEnderecamento;
    }

    function setIdApontamento($idApontamento) {
        $this->idApontamento = $idApontamento;
    }

    function setCodigoPosicao($codigoPosicao) {
        $this->codigoPosicao = $codigoPosicao;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setDataEnderecamento($dataEnderecamento) {
        $this->dataEnderecamento = $dataEnderecamento;
    }



}
