<?php


namespace UBSValorem\Entity;

class ApontamentoGenerico {
    private $idApontamento;
    private $codigoPalete;
    private $paleteOrigem;
    private $tipo;
    private $saldo;
    private $status;
    
    function __construct() {
        
    }

    function getIdApontamento() {
        return $this->idApontamento;
    }

    function getCodigoPalete() {
        return $this->codigoPalete;
    }

    function getPaleteOrigem() {
        return $this->paleteOrigem;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getSaldo() {
        return $this->saldo;
    }

    function getStatus() {
        return $this->status;
    }

    function setIdApontamento($idApontamento) {
        $this->idApontamento = $idApontamento;
    }

    function setCodigoPalete($codigoPalete) {
        $this->codigoPalete = $codigoPalete;
    }

    function setPaleteOrigem($paleteOrigem) {
        $this->paleteOrigem = $paleteOrigem;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setSaldo($saldo) {
        $this->saldo = $saldo;
    }

    function setStatus($status) {
        $this->status = $status;
    }


    
}
