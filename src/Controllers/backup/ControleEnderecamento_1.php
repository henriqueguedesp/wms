<?php

namespace UBSValorem\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UBSValorem\Util\Sessao;
use UBSValorem\Models\ModeloEnderecamento;
use UBSValorem\Models\ModeloApontamento;
use UBSValorem\Entity\Enderecamento;

class ControleEnderecamento {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function realizarEnderecamentoEditar() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $dadosApontamento = $modelo->verificaCodigo($this->request->get('codigoPalete'));
            $atual = $this->request->get('atual');
            $posicao = $this->request->get('codigoPosicao');
            $palete = $this->request->get('codigoPalete');
            if ($posicao == null) {
                echo 10;
            } else {
                $ultimo = strlen($posicao);
                $ultimoC = $posicao[$ultimo - 1];
                $primeiraC = $posicao[0];
                if (($primeiraC == 'P') && ($ultimoC == 'P')) {
                    $modelo = new ModeloEnderecamento();
                    $id = $modelo->editar($palete, $atual, $posicao, $usuario->idUsuario);
                    echo 0;
                } else {
                    echo 10;
                }
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function editarEnderecamento() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $codigo = $this->request->get('codigo');
            $dados = explode('-', $codigo);
            $tamanho = strlen($dados[0]);
            $tipo = substr($dados[0], $tamanho - 2);
            $modelo = new ModeloEnderecamento();
            $verifica = $modelo->verificaCodigo($codigo);
            if ($tipo == 'SC' || $tipo == 'BG') {
                return $this->response->setContent($this->twig->render('EnderecamentoPaleteEditar.html.twig', array('user' => $usuario, 'dados' => $verifica)));
            } else {
                echo "<script> alert('Código lido está incorreto! Por favor verifique!'); "
                . " location.href='/sga/public_html/enderecar';</script>";
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function realizarEnderecamento() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $dadosApontamento = $modelo->verificaCodigo($this->request->get('codigoPalete'));
            $enderecamento = new Enderecamento();
            $enderecamento->setCodigoPosicao($this->request->get('codigoPosicao'));
            $codigo = $this->request->get('codigoPosicao');
            $enderecamento->setIdApontamento($dadosApontamento->idApontamento);
            $enderecamento->setStatus(1);
            $posicao = $this->request->get('codigoPosicao');
            $ultimo = strlen($posicao);
            $ultimoC = $posicao[$ultimo - 1];
            $primeiraC = $posicao[0];
            if (($primeiraC == 'P') && ($ultimoC == 'P')) {
                $dados = explode('-', $codigo);
                $tamanho = strlen($dados[0]);
                $tipo = substr($dados[0], $tamanho - 2);
                if ($tipo == 'PG') {
                    $modelo = new ModeloEnderecamento();
//              /  $id = $modelo->enderecarGenerico($enderecamento, $usuario->idUsuario);
                    echo 0;
                } else {
                    $modelo = new ModeloEnderecamento();
                    $id = $modelo->enderecar($enderecamento, $usuario->idUsuario);
                    echo 0;
                }
            } else {
                echo 10;
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function enderecarPalete() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $codigo = $this->request->get('codigo');
            $dados = explode('-', $codigo);
            $tamanho = strlen($dados[0]);
            $tipo = substr($dados[0], $tamanho - 2);
            if ($tipo == 'SC' || $tipo == 'BG') {

                return $this->response->setContent($this->twig->render('EnderecamentoPalete.html.twig', array('user' => $usuario, 'codigoPalete' => $codigo, 'tipo' => $tipo)));
            } else {
                if ($tipo == 'PG') {
                    return $this->response->setContent($this->twig->render('EnderecamentoPalete.html.twig', array('user' => $usuario, 'codigoPalete' => $codigo, 'tipo' => $tipo)));
                } else {
                    echo "<script> alert('Código lido está incorreto! Por favor verifique!'); "
                    . " location.href='/sga/public_html/enderecar';</script>";
                }
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function verificaPaleteEnderecar() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloEnderecamento();
            $codigo = $this->request->get('codigo');
            $dados = explode('-', $codigo);
            $tamanho = strlen($dados[0]);
            $tipo = substr($dados[0], $tamanho - 2);
            if ($tipo == 'SC' || $tipo == 'BG') {
                $verifica = $modelo->verificaCodigo($codigo);
                if ($verifica) {
                    echo 1;
                } else {
                    $verifica = $modelo->verificaCodigoApontado($codigo);
                    if ($verifica) {
                        echo 0;
                    } else {
                        echo 3;
                    }
                }
            } else {
                if ($tipo == 'PG') {
                    //CÓDIGO DE PALETE GENÉRICO

                    $verifica = $modelo->verificaCodigoGenerico($codigo);
                    if ($verifica) {
                        echo 1;
                    } else {
                        $verifica = $modelo->verificaCodigoApontadoGenerico($codigo);
                        if ($verifica) {
                            echo 0;
                        } else {
                            echo 3;
                        }
                    }
                } else {
                    echo 3;
                }
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function enderecar() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            return $this->response->setContent($this->twig->render('Enderecamento.html.twig', array('user' => $usuario)));
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

}
