<?php

namespace UBSValorem\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UBSValorem\Util\Sessao;
use UBSValorem\Models\ModeloApontamento;
use UBSValorem\Entity\Apontamento;
use UBSValorem\Entity\ApontamentoGenerico;

class ControleApontamento {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    public function reativar() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $codigo = $this->request->get('codigo');
            $verifica = $modelo->verificaCodigoReativar($codigo);
            //if (!$verifica) {
            //    $verifica = $modelo->verificaCodigoGenerico($codigo);
            // }

            if ($codigo == null) {
                echo 2;
            } else {
                if ($verifica) {
                    //  echo 0;
                    // uncao de reativacao
                    $modelo->reativar($codigo, $usuario->idUsuario);
                    echo 0;
                } else {
                    echo 1;
                }
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function reativamento() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            return $this->response->setContent($this->twig->render('ReativarApontamento.html.twig', array('user' => $usuario)));
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function salvarReapontar() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $codigo = $this->request->get('codigo');
            $quantidade = $this->request->get('quantidade');
            $id = $this->request->get('id');
            $tamanho = strlen($codigo);
            $ultimo = $codigo[$tamanho - 1];
            $dados = explode('-', $codigo);
            $tamanho = strlen($dados[0]);
            $apontamento = new Apontamento();

            $apontamento->setCodigoPalete($codigo);
            $apontamento->setIdApontamento($id);
            $apontamento->setSaldo($quantidade);

            $tipo = substr($dados[0], $tamanho - 2);
            if ($tipo == 'SC' || $tipo == 'BG') {
                if ($tipo == 'SC') {
                    $apontamento->setTipo(1);
                } else {
                    $apontamento->setTipo(0);
                }
                $modelo = new ModeloApontamento();
                $dados = $modelo->verificaId($apontamento->getIdApontamento());

                $modelo->editarApontamento($apontamento, $usuario->idUsuario, $dados);
                //print_r($apontamento);
            } else {
                echo 0;
            }

            $modelo = new ModeloApontamento();
            //$dados = $modelo->verificaCodigo($codigo);
            //print_r($dados);
            echo 0;
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function verificaCodigoReapontar() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $codigo = $this->request->get('codigo');
            $id = $this->request->get('id');

            if ($codigo == null) {
                echo 2;
            } else {

                $tamanho = strlen($codigo);
                $ultimo = $codigo[$tamanho - 1];
                $dados = explode('-', $codigo);
                $tamanho = strlen($dados[0]);
                [$tamanho - 1];
                $tipo = substr($dados[0], $tamanho - 2);
                if ($tipo == 'PG') {
                    if ($id) {
                        $verifica = $modelo->verificaCodigoGenericoId($codigo, $id);
                    } else {
                        $verifica = $modelo->verificaCodigoGenerico($codigo);
                    }

                    if ($verifica) {
                        echo 1;
                    } else {
                        $data = $modelo->verificaIdGenerico($id);
                        $modelo->editarApontamentoGenerico($codigo, $data->idApontamentoGenerico, $usuario->idUsuario, $data->codigoPalete);
                        //echo 0;
                        echo -2;
                    }
                } else {
                    if ($id) {
                        $verifica = $modelo->verificaCodigoComId($codigo, $id);
                    } else {
                        $verifica = $modelo->verificaCodigo($codigo);
                    }

                    if ($tipo == 'SC' || $tipo == 'BG') {
                        if ($verifica) {
                            echo 1;
                        } else {
                            echo 0;
                        }
                    } else {

                        echo 3;
                    }
                }
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function reapontarFinal() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $codigo = $this->request->get('codigo');
            $id = $this->request->get('id');
            $modelo = new ModeloApontamento();
            $data = $modelo->verificaId($id);

            $tamanho = strlen($codigo);
            $ultimo = $codigo[$tamanho - 1];
            $dados = explode('-', $codigo);
            $tamanho = strlen($dados[0]);
            $tipo = substr($dados[0], $tamanho - 2);

            return $this->response->setContent($this->twig->render('SalvarReapontar.html.twig', array('user' => $usuario, 'codigo' => $codigo, 'id' => $id, 'tipo' => $tipo, 'dado' => $data)));
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function reapontar() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $codigo = $this->request->get('codigo');
            $modelo = new ModeloApontamento();
            $dados = $modelo->verificaCodigo($codigo);
            if (!$dados) {
                $dados = $modelo->verificaCodigoGenerico($codigo);
            }
            //print_r($dados);
            return $this->response->setContent($this->twig->render('Reapontar.html.twig', array('user' => $usuario, 'dado' => $dados)));
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function verificaCodigoEditar() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $codigo = $this->request->get('codigo');
            $verifica = $modelo->verificaCodigo($codigo);
            if (!$verifica) {
                $verifica = $modelo->verificaCodigoGenerico($codigo);
            }
            if ($codigo == null) {
                echo 2;
            } else {
                if ($verifica) {
                    echo 0;
                } else {
                    echo 1;
                }
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function editarApontamento() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            return $this->response->setContent($this->twig->render('ApontamentoEditar.html.twig', array('user' => $usuario)));
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function verificaPaleteDestinoGenerico() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $origem = $this->request->get('origem');
            $palete = $this->request->get('palete');
            $destino = $this->request->get('destino');
            $quantidade = $this->request->get('quantidade');
//verifica se o paçete de destino é um palete generico
            $dados = explode('-', $destino);
            $tipo = substr($dados[0], -2);
            if ($tipo == 'PG') {
                if ($origem == -1) {
                    echo 11;
                } else {
                    if ($quantidade == null) {
                        echo 12;
                    } else {
                        $apontamento = new ApontamentoGenerico();
                        $modelo = new ModeloApontamento();
                        $apontamento->setCodigoPalete($destino);
                        $apontamento->setPaleteOrigem($origem);
                        $apontamento->setSaldo($quantidade);
                        $apontamento->setStatus(1);
                        //$apontamento->setTipo(1);
                        $modelo->apontarGenericoGenerico($apontamento, $usuario->idUsuario, $palete);
                        echo 1;
                    }
                }
            } else {
                echo 10;
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function apontarGenericoGenerico() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $codigo = $this->request->get('codigo');
            $modelo = new ModeloApontamento();
            $dados = $modelo->materialPaleteGenerico($codigo);
//print_r($dados);           
// print_r('<br><center>--||--</center><br>');           
//  print_r($codigo);           
            return $this->response->setContent($this->twig->render('ApontamentoGenericoGenerico.html.twig', array('user' => $usuario, 'dados' => $dados, 'palete' => $codigo)));
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function verificaPaleteDestino() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $origem = $this->request->get('origem');
            $destino = $this->request->get('destino');
            $quantidade = $this->request->get('quantidade');
//verifica se o paçete de destino é um palete generico
            $dados = explode('-', $destino);
            $tipo = substr($dados[0], -2);
            if ($tipo == 'PG') {
                $apontamento = new ApontamentoGenerico();
                $modelo = new ModeloApontamento();
                $apontamento->setCodigoPalete($destino);
                $apontamento->setPaleteOrigem($origem);
                $apontamento->setSaldo($quantidade);
                $apontamento->setStatus(1);
                //$apontamento->setTipo(1);
                $modelo->apontarGenerico($apontamento, $usuario->idUsuario);
                echo 1;
            } else {
                echo 10;
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function apontarGenerioMatrial() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $codigo = $this->request->get('codigo');
            $modelo = new ModeloApontamento();
            $dados = $modelo->verificaCodigo($codigo);
//            print_r($dados);           
            return $this->response->setContent($this->twig->render('ApontamentoGenericoMaterial.html.twig', array('user' => $usuario, 'palete' => $dados)));
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function verificaPaleteOrigem() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $paleteOrigem = $this->request->get('codigo');
            $dados = explode('-', $paleteOrigem);
//$tipo = substr($dados[2], 0, 2);
            $tipo = substr($dados[0], -2);
            if ($tipo == 'PG') {
                /* $material = substr($dados[0], -2);
                  if ($material == 'SC') {
                  print_r("Palete generico e saco");
                  } else {
                  //operação não permitida
                  if ($material == 'BG') {
                  echo 10;
                  } else {
                  echo 11;
                  }
                  } */
                $modelo = new ModeloApontamento();
                $verifica = $modelo->verificaPaleteGenerico($paleteOrigem);
                if ($verifica) {
                    echo 12;
                } else {
                    echo 13;
                }
            } else {
//PALETE NORMAL
                $material = substr($dados[0], -2);
                if ($material == 'SC') {
                    $modelo = new ModeloApontamento();
                    $dados = $modelo->verificaPaleteEnderecado($paleteOrigem);
//verifica se o palete já foi endereçado
                    if ($dados) {
                        ///COMENTEI ESSA LINHA DA VERIFICAÇÃO
                        // $verifica = $modelo->verificaPaleteExpedicao($paleteOrigem);
                        $verifica = false;
//verifica se o palete já foi expedido
                        if ($verifica) {
//palete já expedido
                            echo 22;
                        } else {
//tudo certo. mostrar página de dados de 
                            echo 0;
                        }
                    } else {
//palete não existe
                        echo 2;
                    }
                } else {
                    if ($material == 'BG') {
                        echo 10;
                    } else {
                        echo 11;
                    }
                }
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function apontarGenerico() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            return $this->response->setContent($this->twig->render('ApontamentoGenerico.html.twig', array('user' => $usuario)));
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function realizarApontamento() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
        sleep(1);
            $modelo = new ModeloApontamento();
            $apontamento = new Apontamento();
            $apontamento->setCodigoPalete($this->request->get('codigoPalete'));

            //verifico se o palete está apontado
            $verifica = $modelo->verificaCodigo($apontamento->getCodigoPalete());
            if ($verifica) {
                echo 1;
            } else {


                $apontamento->setSaldo($this->request->get('quantidade'));
                $apontamento->setStatus(1);
                $dados = explode('-', $apontamento->getCodigoPalete());
                $tamanho = strlen($dados[0]);
                $tipo = substr($dados[0], $tamanho - 2);
                if ($tipo == 'SC') {
                    $apontamento->setTipo(1);
                } else {
                    $apontamento->setTipo(0);
                }
                $id = $modelo->apontar($apontamento, $usuario->idUsuario);
                /*
                 * CÓDIGO COMENTADO DIA 28/08/2018 AS 14:32 PARA MELHORIA DO SISTEMA PARA EVITAR O DUPLICIDADE DE APONTAMENTOS
                  echo "<script> alert('Apontamento realizado com sucesso!');"
                  . "location.href='/sga/public_html/';</script>"; */


                //return $this->response->setContent($this->twig->render('ApontamentoMaterial.html.twig', array('user' => $usuario, 'codigoPalete' => $codigo, 'tipo' => $tipo)));

                echo 0;
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function apontarPalete() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $codigo = $this->request->get('codigoPaleteSalvar');
            if ($codigo == null || $codigo == "") {
                $this->redireciona('/sga/public_html/apontar');
            } else {

                $dados = explode('-', $codigo);
                $tamanho = strlen($dados[0]);
                $tipo = substr($dados[0], $tamanho - 2);
// if ($tipo == 'SC' || $tipo == 'BG') {
                return $this->response->setContent($this->twig->render('ApontamentoMaterial.html.twig', array('user' => $usuario, 'codigoPalete' => $codigo, 'tipo' => $tipo)));
// } else {
//     echo "<script> alert('Código lido está incorreto! Por favor verifique!'); "
///      . " location.href='/sga/public_html/apontar';</script>";
// }
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function verificaCodigo() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $codigo = $this->request->get('codigo');
            $id = $this->request->get('id');
            if ($id) {
                $verifica = $modelo->verificaCodigoComId($codigo, $id);
            } else {
                $verifica = $modelo->verificaCodigo($codigo);
            }
            if ($codigo == null) {
                echo 2;
            } else {
                $tamanho = strlen($codigo);
                $ultimo = $codigo[$tamanho - 1];
                $dados = explode('-', $codigo);
                $tamanho = strlen($dados[0]);
                $tipo = substr($dados[0], $tamanho - 2);
                if (($tipo == 'SC' && $ultimo == 'P')) {
                    if ($verifica) {
                        echo 1;
                    } else {
                        echo 0;
                    }
                } else {
                    if (($tipo == 'BG' && $ultimo == 'B')) {
                        if ($verifica) {
                            echo 1;
                        } else {
                            echo 0;
                        }
                    } else {
                        echo 3;
                    }
                }
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function apontar() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            return $this->response->setContent($this->twig->render('Apontamento.html.twig', array('user' => $usuario)));
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

}
