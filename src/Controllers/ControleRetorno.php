<?php

namespace UBSValorem\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UBSValorem\Util\Sessao;
use UBSValorem\Models\ModeloRetorno;
use UBSValorem\Models\ModeloApontamento;
use UBSValorem\Models\ModeloEnderecamento;
use UBSValorem\Entity\OrdemRetorno;
use UBSValorem\Entity\Retorno;

class ControleRetorno {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function finalizarOrdemRetorno() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $codigoOrdem = $this->request->get('codigoOrdemFinalizar');
            $quantidade = $this->request->get('quantidade');

                $modelo = new ModeloRetorno();
                $modelo->finalizarOrdem($codigoOrdem, $quantidade, $usuario->idUsuario);
                echo 0;
          
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function verificaExpedirPaleteRetorno() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $codigo = $this->request->get('codigoOrdem');
            $palete = $this->request->get('codigoPalete');
            $modelo = new ModeloRetorno();
            $dados = $modelo->verificaCodigoOrdemRetorno($codigo);
            if ($codigo != $palete) {
                $data = explode('-', $palete);

                $tamanho = strlen($data[0]);
                $tipo = substr($data[0], $tamanho - 2);
                if ($tipo == 'BG') {
                    if ($dados->codigoLote == $data[1]) {
                        $modelo = new ModeloApontamento();
                        $verifica = $modelo->verificaCodigo($palete);
                        if ($verifica) {
                            //o bag está apontado e o status é 1
                            $modelo = new ModeloEnderecamento();
                            $verifica = $modelo->verificaCodigo($palete);
                            if ($verifica) {
                                //o palete está enderecado e pode realizar a operação de retorno
                                $modelo = new ModeloApontamento();
                                $verifica = $modelo->verificaCodigo($palete);
                                $retorno = new Retorno();
                                $retorno->setIdApontamento($verifica->idApontamento);
                                $retorno->setIdUsuario($usuario->idUsuario);
                                $modelo = new ModeloRetorno();
                                $dados = $modelo->verificaCodigoOrdemRetorno($codigo);
                                $retorno->setIdOrdem($dados->idOrdemRetorno);

                                $modelo->expedir($retorno);
                                echo 0;
                            } else {
                                //palete não enderecado 
                                echo 13;
                            }
                        } else {
                            //o bag não está apontado ou o status é 0
                            echo 12;
                        }
                    } else {
                        //LOTE DO PALETE DIFERENTE DO LOTE DA ORDEM DE RETORNO
                        echo 11;
                    }
                } else {
                    //material não é BAG
                    echo 10;
                }
            } else {
                echo 30;
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function retornarPalete() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $codigo = $this->request->get('codigo');

            return $this->response->setContent($this->twig->render('ExpedicaoPaleteRetorno.html.twig', array('user' => $usuario, 'ordem' => $codigo)));
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function verificaCodigoOrdemRetorno() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $codigo = $this->request->get('codigo');
            $tipo = substr($codigo, 0, 3);
            if ($tipo == 'EXR') {
                $modelo = new ModeloRetorno();
                $dados = $modelo->verificaCodigoOrdemRetorno($codigo);
                if ($dados) {

                    echo 0;
                } else {
                    echo 1;
                }
            } else {
                echo 2;
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function paginaRetorno() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            return $this->response->setContent($this->twig->render('ExpedicaoRetorno.html.twig', array('user' => $usuario)));
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function editarOrdemRetorno() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario && $usuario->tipo == 1) {
            $codigoOrdem = $this->request->get('codigoOrdem');
            $numeroLote = $this->request->get('numeroLote');
            $id = $this->request->get('id');
            $ordemRetorno = new OrdemRetorno();
            $ordemRetorno->setCodigoLote($numeroLote);
            $ordemRetorno->setCodigoRetorno($codigoOrdem);
            $ordemRetorno->setIdOrdemRetorno($id);
            $ordemRetorno->setIdUsuario($usuario->idUsuario);
            $tipo = substr($codigoOrdem, 0, 3);
            if ($tipo == 'EXR') {

                $modelo = new ModeloRetorno();
                $dados = $modelo->verificaOrdemRetornoEditar($codigoOrdem, $id);
                if ($dados) {
                    echo 1;
                } else {
                    $dados = $modelo->ordem($id);
                  $kk = $modelo->editarOrdem($ordemRetorno, $dados);
                    echo 0;
                }
            } else {
                echo 2;
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function cadastrarOrdemRetorno() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario && $usuario->tipo == 1) {
            $codigoOrdem = $this->request->get('codigoOrdem');
            $numeroLote = $this->request->get('numeroLote');
            $ordemRetorno = new OrdemRetorno();
            $ordemRetorno->setCodigoLote($numeroLote);
            $ordemRetorno->setCodigoRetorno($codigoOrdem);
            $ordemRetorno->setIdUsuario($usuario->idUsuario);
            $tipo = substr($codigoOrdem, 0, 3);
            if ($tipo == 'EXR') {

                $modelo = new ModeloRetorno();
                $dados = $modelo->verificaOrdemRetorno($codigoOrdem);
                if ($dados) {
                    echo 1;
                } else {
                    $dados = $modelo->cadastrarOrdemRetorno($ordemRetorno);
                    echo 0;
                }
            } else {
                echo 2;
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function controleRetorno() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloRetorno();
            $dados = $modelo->ordens();
            return $this->response->setContent($this->twig->render('ControleRetorno.html.twig', array('user' => $usuario, 'dados' => $dados)));
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

}
