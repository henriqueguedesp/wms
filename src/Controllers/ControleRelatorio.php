<?php

namespace UBSValorem\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UBSValorem\Util\Sessao;
use UBSValorem\Models\ModeloApontamento;
use UBSValorem\Models\ModeloRetorno;
use UBSValorem\Entity\Apontamento;
use UBSValorem\Entity\ApontamentoGenerico;

class ControleRelatorio {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function relatorioPaletePorExpedicao() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $codigoExpedicao = $this->request->get('ordemExpedicao');
            $modelo = new ModeloApontamento();
            $dados = $modelo->relatorioPaletePorExpedicao($codigoExpedicao);

            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatorio_Palete_X_Ordem_Expedicao.xls");

            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table border='1'>";
            echo "<tr>";
            echo "<td><b>Ordem Expedição</b></td>";
            echo "<td><b>Código Palete</b></td>";
            echo "<td><b>Lote</b></td>";
            echo "<td><b>Quantidade</b></td>";
            echo "<td><b>Check</b></td>";
            echo "</tr>";
            foreach ($dados as $dado) {
                echo "<tr>";
                echo "<td>" . $dado->ordemExpedicao . "</td>";
                echo "<td>" . $dado->codigoPalete . "</td>";
                $aux = explode('-', $dado->codigoPalete);
                echo "<td>" . $aux[1] . "</td>";
                echo "<td>" . $dado->saldo . "</td>";
                echo "<td>    </td>";
                echo "</tr>";
            }
            echo "</table>";
            
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function relatorios() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            return $this->response->setContent($this->twig->render('Relatorios.html.twig', array('user' => $usuario)));
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function relatorioTodosApontamentos() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $dados = $modelo->todosApontamentos();

            //header("Content-type: application/vnd.ms-excel");
            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatorio_Todos_Apontamentos.xls");

            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table border='1'>";
            echo "<tr>";
            echo "<td><b>Código Palete</b></td>";
            echo "<td><b>Tipo</b></td>";
            echo "<td><b>Saldo</b></td>";
            echo "<td><b>Status</b></td>";
            echo "<td><b>Responsável Apontamento</b></td>";
            echo "<td><b>Data Apontamento</b></td>";
            echo "</tr>";
            foreach ($dados as $dado) {
                echo "<tr>";
                echo "<td>" . $dado->codigoPalete . "</td>";
                if ($dado->tipo == 1) {
                    echo "<td> SACO</td>";
                } else {
                    echo "<td> BAG</td>";
                }

                echo "<td>" . $dado->saldo . "</td>";
                echo "<td>" . $dado->status . "</td>";
                echo "<td>" . $dado->nome . "</td>";
                echo "<td>" . $dado->data . "</td>";


                echo "</tr>";
            }
            echo "</table>";
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function relatorioRetornoProcesso() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloRetorno();
            $dados = $modelo->relatorio();

            //header("Content-type: application/vnd.ms-excel");
            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatorio_Retorno_Processo.xls");

            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table border='1'>";
            echo "<tr>";
            echo "<td><b>Código Ordem Retorno</b></td>";
            echo "<td><b>Código Palete</b></td>";
            echo "<td><b>Código Posição</b></td>";
            echo "<td><b>Tipo</b></td>";
            echo "<td><b>Saldo</b></td>";
            echo "<td><b>Data</b></td>";
            echo "</tr>";
            foreach ($dados as $dado) {
                echo "<tr>";
                echo "<td>" . $dado->codigoRetorno . "</td>";
                echo "<td>" . $dado->codigoPalete . "</td>";
                echo "<td>" . $dado->idPosicao . "</td>";
                if ($dado->tipo == 1) {
                    echo "<td> SACO</td>";
                } else {
                    echo "<td> BAG</td>";
                }

                echo "<td>" . $dado->saldo . "</td>";
                echo "<td>" . $dado->data . "</td>";


                echo "</tr>";
            }
            echo "</table>";
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function relatorioExpedicaoGenerico() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $dados = $modelo->relatorioExpedicaoGenerico();

            //header("Content-type: application/vnd.ms-excel");
            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatorio_Palete_Generico_Expedicao.xls");

            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table border='1'>";
            echo "<tr>";
            echo "<td><b>Código Ordem</b></td>";
            echo "<td><b>Código Palete</b></td>";
            echo "<td><b>Código Palete Origem</b></td>";
            echo "<td><b>Código Posição</b></td>";
            echo "<td><b>Tipo</b></td>";
            echo "<td><b>Saldo</b></td>";
            echo "</tr>";
            foreach ($dados as $dado) {
                echo "<tr>";
                echo "<td>" . $dado->ordemExpedicao . "</td>";
                echo "<td>" . $dado->codigoPalete . "</td>";
                echo "<td>" . $dado->paleteOrigem . "</td>";
                echo "<td>" . $dado->idPosicao . "</td>";


                echo "<td>" . $dado->saldo . "</td>";
                echo "<td> PALETE GENÉRICO</td>";


                echo "</tr>";
            }
            echo "</table>";
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function relatorioExpedicao() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $dados = $modelo->relatorioExpedicao();

            //header("Content-type: application/vnd.ms-excel");
            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatorio_Palete_Expedicao.xls");

            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table border='1'>";
            echo "<tr>";
            echo "<td><b>Código Ordem</b></td>";
            echo "<td><b>Código Palete</b></td>";
            echo "<td><b>Código Posição</b></td>";
            echo "<td><b>Tipo</b></td>";
            echo "<td><b>Saldo</b></td>";
            echo "<td><b>Data Expedição</b></td>";
            echo "<td><b>Responsável Expedição</b></td>";
            echo "</tr>";
            foreach ($dados as $dado) {
                echo "<tr>";
                echo "<td>" . $dado->ordemExpedicao . "</td>";
                echo "<td>" . $dado->codigoPalete . "</td>";
                echo "<td>" . $dado->idPosicao . "</td>";
                if ($dado->tipo == 1) {
                    echo "<td> SACO</td>";
                } else {
                    echo "<td> BAG</td>";
                }

                echo "<td>" . $dado->saldo . "</td>";
                echo "<td>" . $dado->date . "</td>";
                echo "<td>" . $dado->nome . "</td>";


                echo "</tr>";
            }
            echo "</table>";
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function relatorioPaleteApontado() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $dados = $modelo->relatorioPaleteApontado();

            //header("Content-type: application/vnd.ms-excel");
            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatorio_Palete_Apontado.xls");

            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table border='1'>";
            echo "<tr>";
            echo "<td><b>Código Palete</b></td>";
            echo "<td><b>Saldo</b></td>";
            echo "<td><b>Tipo</b></td>";
            echo "</tr>";
            foreach ($dados as $dado) {
                echo "<tr>";
                echo "<td>" . $dado->codigoPalete . "</td>";
                echo "<td>" . $dado->saldo . "</td>";
                if ($dado->tipo == 1) {
                    echo "<td> SACO</td>";
                } else {
                    echo "<td> BAG</td>";
                }

                echo "</tr>";
            }
            echo "</table>";
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function relatorioPaleteApontadoGenerico() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $dados = $modelo->relatorioPaleteApontadoGenerico();

            //header("Content-type: application/vnd.ms-excel");
            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatorio_Palete_Generico_Apontado.xls");

            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table border='1'>";
            echo "<tr>";
            echo "<td><b>Código Palete</b></td>";
            echo "<td><b>Código Palete Origem</b></td>";
            echo "<td><b>Saldo</b></td>";
            echo "<td><b>Tipo</b></td>";
            echo "</tr>";
            foreach ($dados as $dado) {
                echo "<tr>";
                echo "<td>" . $dado->codigoPalete . "</td>";
                echo "<td>" . $dado->paleteOrigem . "</td>";
                echo "<td>" . $dado->saldo . "</td>";
                echo "<td> PALETE GENÉRICO </td>";


                echo "</tr>";
            }
            echo "</table>";
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function relatorioPaleteEnderecado() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $dados = $modelo->relatorioPaleteEnderecado();

            //header("Content-type: application/vnd.ms-excel");
            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatorio_Palete_Enderecado.xls");

            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table border='1'>";
            echo "<tr>";
            echo "<td><b>Código Palete</b></td>";
            echo "<td><b>Código Posição</b></td>";
            echo "<td><b>Saldo</b></td>";
            echo "<td><b>Tipo</b></td>";
            echo "<td><b>Data Endereçamento</b></td>";
            echo "<td><b>Responsável Endereçamento</b></td>";
            echo "</tr>";
            foreach ($dados as $dado) {
                echo "<tr>";
                echo "<td>" . $dado->codigoPalete . "</td>";
                echo "<td>" . $dado->idPosicao . "</td>";
                echo "<td>" . $dado->saldo . "</td>";
                if ($dado->tipo == 1) {
                    echo "<td> SACO</td>";
                } else {
                    echo "<td> BAG</td>";
                }
                if ($dado->usuarioReenderecamento != null){
                	                echo "<td>" . $dado->dataReenderecamento . "</td>";

                    echo "<td>" . $dado->usuarioReenderecamento . "</td>";
               } else {
                	                echo "<td>" . $dado->date . "</td>";

                    echo "<td>" . $dado->nome . "</td>";
                }
                echo "</tr>";
            }
            echo "</table>";
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function relatorioPaleteEnderecadoGenerico() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $dados = $modelo->relatorioPaleteGenericoEnderecado();

            //header("Content-type: application/vnd.ms-excel");
            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: application/x-msexcel');
            header("Content-Disposition: attachment; filename = Relatorio_Palete_Generico_Enderecado.xls");

            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "<table border='1'>";
            echo "<tr>";
            echo "<td><b>Código Palete</b></td>";
            echo "<td><b>Código Posição</b></td>";
            echo "<td><b>Código Palete Origem</b></td>";
            echo "<td><b>Saldo</b></td>";
            echo "<td><b>Tipo</b></td>";
            echo "</tr>";
            foreach ($dados as $dado) {
                echo "<tr>";
                echo "<td>" . $dado->codigoPalete . "</td>";
                echo "<td>" . $dado->idPosicao . "</td>";
                echo "<td>" . $dado->paleteOrigem . "</td>";
                echo "<td>" . $dado->saldo . "</td>";
                echo "<td> PALETE GENÉRICO</td>";


                echo "</tr>";
            }
            echo "</table>";
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

}
