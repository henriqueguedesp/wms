<?php

namespace UBSValorem\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use UBSValorem\Util\Sessao;
use UBSValorem\Models\ModeloSala;
use UBSValorem\Models\ModeloAgendamento;
use UBSValorem\Models\ModeloEmail;

class ControleIndex {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function erro404() {
        //$modelo = new ModeloEmail();
        // $modelo->enviarComunicado();
        return $this->response->setContent($this->twig->render('Erro404.html.twig'));
    }

    public function controleSala() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario && $usuario->tipo == 1) {
            $modelo = new ModeloSala();
            $salas = $modelo->salas();
            //return $this->response->setContent($this->twig->render('Dashboard.html.twig', array('dados' => $dados, 'user' => $usuario)));
            return $this->response->setContent($this->twig->render('ControleSala.html.twig', array('user' => $usuario, 'salas' => $salas)));
        } else {
            $this->redireciona('/sas/public_html/login');
        }
    }

    public function dashboard() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            return $this->response->setContent($this->twig->render('Dashboard.html.twig', array('user' => $usuario)));
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

}
