<?php

namespace UBSValorem\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UBSValorem\Util\Sessao;
use UBSValorem\Models\ModeloExpedicao;
use UBSValorem\Models\ModeloEnderecamento;
use UBSValorem\Entity\Expedicao;

class ControleExpedicao {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function finalizarOrdem() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $codigoOrdem = $this->request->get('codigoOrdemFinalizar');
            $pesoOrdem = $this->request->get('pesoOrdem');
            $nota = $this->request->get('nota');

            //echo "<script> alert('Ordem finalizada com sucesso!'); "
            //. " location.href='/sga/public_html/';</script>";
            if ($pesoOrdem && $nota) {
                $modelo = new ModeloExpedicao();
                $modelo->finalizarOrdem($codigoOrdem, $pesoOrdem, $usuario->idUsuario, $nota);
                echo 0;
            } else {
                if (!$pesoOrdem && !$nota) {
                    echo 22;
                } else {
                    if (!$pesoOrdem) {
                        echo 23;
                    } else {
                        if (!$nota) {
                            echo 24;
                        }
                    }
                }
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function verificaExpedirPalete() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloEnderecamento();
            $codigoOrdem = $this->request->get('codigoOrdem');
            $codigoPalete = $this->request->get('codigoPalete');

            $tipo = substr($codigoPalete, 0, 2);
            if ($tipo == 'EX') {
                if ($codigoOrdem == $codigoPalete) {
                    echo 30;
                } else {
                    echo 10;
                }
            } else {
                $dados = explode('-', $codigoPalete);
                $tamanho = strlen($dados[0]);
                $tipo = substr($dados[0], $tamanho - 2);
                if ($tipo == 'SC' || $tipo == 'BG') {
                    $verifica = $modelo->verificaDisponivel($codigoPalete);
                    if ($verifica) {
                        $modelo = new ModeloExpedicao();
                        $expedicao = new Expedicao();
                        $expedicao->setCodigoPalete($codigoPalete);
                        $expedicao->setOrdemExpedicao($codigoOrdem);
                        $id = $modelo->expedir($expedicao, $usuario->idUsuario);

//  if ($finalizar == 'sim') {
//      $modelo->finalizarOrdem($codigoOrdem);
//  }
                        echo 1;
//
                    } else {

                        echo 0;
                    }
                } else {
                    if ($tipo == 'PG') {
                        $verifica = $modelo->verificaDisponivelGenerico($codigoPalete);
                        if ($verifica) {
                            $modelo = new ModeloExpedicao();
                            $expedicao = new Expedicao();
                            $expedicao->setCodigoPalete($codigoPalete);
                            $expedicao->setOrdemExpedicao($codigoOrdem);
                            $id = $modelo->expedirGenerico($expedicao, $usuario->idUsuario);

//     if ($finalizar == 'sim') {
//       $modelo->finalizarOrdem($codigoOrdem);
//   }
                            echo 1;
                        } else {

                            echo 0;
                        }
                    } else {
                        echo 10;
                    }
                }
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function expedirPalete() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $ordemExpedicao = $this->request->get('codigo');

            return $this->response->setContent($this->twig->render('ExpedicaoPalete.html.twig', array('user' => $usuario, 'ordem' => $ordemExpedicao)));
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function verificaCodigoOrdem() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloExpedicao();
            $codigo = $this->request->get('codigo');
            $verifica = $modelo->verificaCodigo($codigo);
            if ($verifica) {
                echo 1;
            } else {

                $comeco = $codigo[0] . $codigo[1];
                if ($comeco == 'EX') {
                    echo 0;
                } else {
                    echo 3;
                }
            }
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function expedir() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            return $this->response->setContent($this->twig->render('Expedicao.html.twig', array('user' => $usuario)));
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

}
