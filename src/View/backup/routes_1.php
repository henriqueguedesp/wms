<?php

namespace UBSValorem\Routes;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$rotas = new RouteCollection();

$rotas->add('raiz', new Route('/', array('_controller' => 
    'UBSValorem\Controllers\ControleIndex',
    '_method' => 'dashboard'))); 

$rotas->add('controleSala', new Route('/controleSala', array('_controller' => 
    'UBSValorem\Controllers\ControleIndex',
    '_method' => 'controleSala'))); 

##LOGIN
$rotas->add('login', new Route('/login  ', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'paginaLogin'))); 

$rotas->add('validaLogin', new Route('/validaLogin', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'validaLogin'))); 

$rotas->add('removerUsuario', new Route('/removerUsuario', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'removerUsuario'))); 

##CONTROLE DE SALAS

$rotas->add('cadastroDeSala', new Route('/cadastrarSala', array('_controller' => 
    'UBSValorem\Controllers\ControleSala',
    '_method' => 'cadastrarSala'))); 

$rotas->add('editarSala', new Route('/editarSala/{_param}', array('_controller' => 
    'UBSValorem\Controllers\ControleSala',
    '_method' => 'editarSala'))); 

$rotas->add('ativarSala', new Route('/ativarSala/{_param}', array('_controller' => 
    'UBSValorem\Controllers\ControleSala',
    '_method' => 'ativarSala'))); 

$rotas->add('desativarSala', new Route('/desativarSala/{_param}', array('_controller' => 
    'UBSValorem\Controllers\ControleSala',
    '_method' => 'desativarSala'))); 


##AGENDAMENTO
$rotas->add('agendamento', new Route('/agendamento', array('_controller' => 
    'UBSValorem\Controllers\ControleAgendamento',
    '_method' => 'agendamento'))); 

$rotas->add('realizandoAgendamento', new Route('/realizandoAgendamento', array('_controller' => 
    'UBSValorem\Controllers\ControleAgendamento',
    '_method' => 'realizandoAgendamento'))); 

$rotas->add('realizarAgendamento', new Route('/realizarAgendamento', array('_controller' => 
    'UBSValorem\Controllers\ControleAgendamento',
    '_method' => 'realizarAgendamento'))); 

$rotas->add('cancelarAgendamento', new Route('/cancelarAgendamento/{_param}', array('_controller' => 
    'UBSValorem\Controllers\ControleAgendamento',
    '_method' => 'cancelarAgendamento'))); 

$rotas->add('editarAgendamento', new Route('/editarAgendamento', array('_controller' => 
    'UBSValorem\Controllers\ControleAgendamento',
    '_method' => 'editarAgendamento'))); 

##CONTROLE DE USUÁRIO
$rotas->add('controleUsuario', new Route('/controleUsuario', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'controleUsuario'))); 

$rotas->add('cadastrarUsuario', new Route('/cadastrarUsuario', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'cadastrarUsuario'))); 

$rotas->add('editarUsuario', new Route('/editarUsuario/{_param}', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'editarUsuario'))); 

$rotas->add('ativarUsuario', new Route('/ativarUsuario/{_param}', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'ativarUsuario'))); 

$rotas->add('desativarUsuario', new Route('/desativarUsuario/{_param}', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'desativarUsuario'))); 

$rotas->add('erro404', new Route('/erro404', array('_controller' => 
    'UBSValorem\Controllers\ControleIndex',
    '_method' => 'erro404'))); 

##CONTROLE PERFIL
$rotas->add('controlePerfil', new Route('/controlePerfil', array('_controller' => 
    'UBSValorem\Controllers\ControlePerfil',
    '_method' => 'paginaPerfil'))); 

$rotas->add('atualizarPerfil', new Route('/atualizarPerfil', array('_controller' => 
    'UBSValorem\Controllers\ControlePerfil',
    '_method' => 'atualizarPerfil'))); 

return $rotas;
