<?php

namespace UBSValorem\Models;

use UBSValorem\Util\Conexao;
use PDO;
use UBSValorem\Entity\Apontamento;
use UBSValorem\Entity\ApontamentoGenerico;

class ModeloApontamento {

    function __construct() {
        
    }

    public function relatorioPaletePorExpedicao($codigoExpedicao) {
        try {

            //$sql = "select * from apontamento where  status = 1 and saldo > 0";
            //    $sql = "select ordemExpedicao,codigoPalete from expedicao;";
            $sql = 'SELECT e.ordemExpedicao, e.codigoPalete, a.saldo 
                    FROM expedicao AS e, apontamento AS a
                    WHERE e.ordemExpedicao = :codigoExpedicao and e.codigoPalete  = a.codigoPalete';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigoExpedicao', $codigoExpedicao);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function reativar($codigo, $idUsuario) {
        try {
            $conexao = Conexao::getInstance();

            $status = "update apontamento as a, enderecamento as e set e.status = 1, a.status = 1 "
                    . " where e.idApontamento = a.idApontamento and a.codigoPalete = :palete;";
            $conexao->beginTransaction();
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':palete', $codigo);
            $p_status->execute();

            $sql = "select * from apontamento where  codigoPalete = :codigo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);
            $p_sql->execute();
            $codigo = $p_sql->fetch(PDO::FETCH_OBJ);



            $status = "insert into historicoReativamento (idApontamento, idUsuario, data)"
                    . " values "
                    . "(:idApontamento, :idUsuario,now())";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->bindValue(':idApontamento', $codigo->idApontamento);
            $p_status->execute();

            $conexao->commit();
        } catch (Exception $ex) {
            
        }
    }

    public function editarApontamentoGenerico($codigoGenerico, $idGenerico, $idUsuario, $codigoAntigo) {
        //apontamento para apontamento Genrico
        try {
            $conexao = Conexao::getInstance();
            //$sql = "insert into apontamentoGenerico (codigoPalete, paleteOrigem, tipo, saldo, status)"
            $sql = "update apontamentoGenerico set codigoPalete = :palete where "
                    . " codigoPalete = :codigoAntigo";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':palete', $codigoGenerico);
            $p_sql->bindValue(':codigoAntigo', $codigoAntigo);
            $p_sql->execute();

            $status = "insert into historicoReapontamento(idApontamento, idUsuario, tipo, data, codigoPalete, saldo,  "
                    . "tipoPalete )"
                    . " values "
                    . "(:idApontamento, :idUsuario,0,now(), :codigoPalete, -1000, -2)";

            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idApontamento', $idGenerico);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->bindValue(':codigoPalete', $codigoAntigo);
            $p_status->execute();

            $conexao->commit();
        } catch (Exception $ex) {
            
        }
    }

    public function editarApontamento(Apontamento $apontamento, $idUsuario, $apontamentoAntigo) {
        //apontamento para apontamento Genrico
        try {
            $conexao = Conexao::getInstance();
            //$sql = "insert into apontamentoGenerico (codigoPalete, paleteOrigem, tipo, saldo, status)"
            $sql = "update apontamento set codigoPalete = :palete, tipo = :tipo, saldo = :saldo where "
                    . " idApontamento = :id";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':palete', $apontamento->getCodigoPalete());
            $p_sql->bindValue(':tipo', $apontamento->getTipo());
            $p_sql->bindValue(':saldo', $apontamento->getSaldo());
            $p_sql->bindValue(':id', $apontamento->getIdApontamento());
            $p_sql->execute();

            $status = "insert into historicoReapontamento(idApontamento, idUsuario, tipo, data, codigoPalete, saldo,  "
                    . "tipoPalete )"
                    . " values "
                    . "(:idApontamento, :idUsuario,1,now(), :codigoPalete, :saldo, :tipoPalete)";

            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idApontamento', $apontamento->getIdApontamento());
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->bindValue(':codigoPalete', $apontamentoAntigo->codigoPalete);
            $p_status->bindValue(':saldo', $apontamentoAntigo->saldo);
            $p_status->bindValue(':tipoPalete', $apontamentoAntigo->tipo);
            $p_status->execute();

            $conexao->commit();
        } catch (Exception $ex) {
            
        }
    }

    public function apontarGenericoGenerico(ApontamentoGenerico $apontamento, $idUsuario, $palete) {
        //apontamento para apontamento Genrico
        try {
            $conexao = Conexao::getInstance();
            //$sql = "insert into apontamentoGenerico (codigoPalete, paleteOrigem, tipo, saldo, status)"
            $sql = "insert into apontamentoGenerico (codigoPalete, paleteOrigem, saldo, status) "
                    . " values "
                    //. " (:cod, :origem, :tipo, :saldo, :status)";
                    . " (:cod, :origem, :saldo, :status)";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':cod', $apontamento->getCodigoPalete());
            $p_sql->bindValue(':origem', $apontamento->getPaleteOrigem());
            //$p_sql->bindValue(':tipo', $apontamento->getTipo());
            $p_sql->bindValue(':saldo', $apontamento->getSaldo());
            $p_sql->bindValue(':status', $apontamento->getStatus());
            $p_sql->execute();
            $id = Conexao::getInstance()->lastInsertId();

            $saldo = "update apontamentoGenerico set saldo = saldo - :quant where codigoPalete = :palete and paleteOrigem = :origem";
            $p_saldo = $conexao->prepare($saldo);
            $p_saldo->bindValue(':quant', $apontamento->getSaldo());
            $p_saldo->bindValue(':palete', $palete);
            $p_saldo->bindValue(':origem', $apontamento->getPaleteOrigem());
            $p_saldo->execute();


            $veri = "update apontamentoGenerico set status = 0 where saldo = 0";
            $p_veri = $conexao->prepare($veri);

            $p_veri->execute();

            $status = "insert into historicoApontamentoGenerico (idApontamentoGenerico, idUsuario, data)"
                    . " values "
                    . "(:idApontamento, :idUsuario,now())";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idApontamento', $id);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->execute();


            $conexao->commit();
        } catch (Exception $ex) {
            
        }
    }

    public function materialPaleteGenerico($codigo) {
        try {

            $sql = "select * from apontamentoGenerico where  status = 1 and codigoPalete = :codigo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function verificaPaleteGenerico($codigo) {
        try {

            $sql = "select * from apontamentoGenerico where codigoPalete = :codigo and status = 1 ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function apontarGenerico(ApontamentoGenerico $apontamento, $idUsuario) {
        //apontamento para apontamento Genrico
        try {
            $conexao = Conexao::getInstance();
//            $sql = "insert into apontamentoGenerico (codigoPalete, paleteOrigem, tipo, saldo, status)"
            $sql = "insert into apontamentoGenerico (codigoPalete, paleteOrigem,  saldo, status)"
                    . " values"
                    //. " (:cod, :origem, :tipo, :saldo, :status)";
                    . " (:cod, :origem, :saldo, :status)";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':cod', $apontamento->getCodigoPalete());
            $p_sql->bindValue(':origem', $apontamento->getPaleteOrigem());
            //$p_sql->bindValue(':tipo', $apontamento->getTipo());
            $p_sql->bindValue(':saldo', $apontamento->getSaldo());
            $p_sql->bindValue(':status', $apontamento->getStatus());
            $p_sql->execute();
            $id = Conexao::getInstance()->lastInsertId();

            $saldo = "update apontamento set saldo = saldo - :quant where codigoPalete = :palete";
            $p_saldo = $conexao->prepare($saldo);
            $p_saldo->bindValue(':quant', $apontamento->getSaldo());
            $p_saldo->bindValue(':palete', $apontamento->getPaleteOrigem());
            $p_saldo->execute();


            $veri = "update apontamento set status = 0 where saldo = 0";
            $p_veri = $conexao->prepare($veri);

            $p_veri->execute();

            $status = "insert into historicoApontamentoGenerico (idApontamentoGenerico, idUsuario, data)"
                    . " values "
                    . "(:idApontamento, :idUsuario,now())";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idApontamento', $id);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->execute();


            $conexao->commit();
        } catch (Exception $ex) {
            
        }
    }

    public function verificaPaleteEnderecado($codigo) {
        try {

            $sql = "select * from apontamento as a, enderecamento as e where a.idApontamento= e.idApontamento and a.codigoPalete = :codigo and a.status = 1 and a.saldo > 0";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function verificaPaleteExpedicao($codigo) {
        //metodo que irá verificar se o palete já foi expedido
        try {

            $sql = "select * from apontamento as a, expedicao as e where a.codigoPalete = e.codigoPalete and a.codigoPalete =  :codigo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function apontar(Apontamento $apontamento, $idUsuario) {
        try {
            $conexao = Conexao::getInstance();
            $sql = "insert into apontamento (codigoPalete, tipo, saldo, status)"
                    . " values"
                    . " (:cod, :tipo, :saldo, :status)";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':cod', $apontamento->getCodigoPalete());
            $p_sql->bindValue(':tipo', $apontamento->getTipo());
            $p_sql->bindValue(':saldo', $apontamento->getSaldo());
            $p_sql->bindValue(':status', $apontamento->getStatus());
            $p_sql->execute();
            $id = Conexao::getInstance()->lastInsertId();

            $status = "insert into historicoApontamento (idApontamento, idUsuario, data)"
                    . " values "
                    . "(:idApontamento, :idUsuario,now())";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idApontamento', $id);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->execute();

            $conexao->commit();
        } catch (Exception $ex) {
            
        }
    }

    public function verificaCodigoGenerico($codigo) {
        try {
            //codígo comentado dia 03/07/18 para comparar em todos paletes não dependendo do status
            //$sql = "select * from apontamentoGenerico where   codigoPalete = :codigo";
            $sql = "select * from apontamentoGenerico where  status = 1 and codigoPalete = :codigo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function verificaCodigoGenericoId($codigo, $id) {
        try {
            //codígo comentado dia 03/07/18 para comparar em todos paletes não dependendo do status
//            $sql = "select * from apontamentoGenerico where  status = 1 and codigoPalete = :codigo and idApontamentoGenerico != :id";
            $sql = "select * from apontamentoGenerico where   codigoPalete = :codigo and idApontamentoGenerico != :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);
            $p_sql->bindValue(':id', $id);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function relatorioPaleteGenericoEnderecado() {
        try {

            //$sql = "select * from apontamento where  status = 1 and saldo > 0";
//na query abaixo ela so tras os paletes normais            
//$sql = "select a.codigoPalete, a.tipo, a.saldo, e.idPosicao from apontamento as a, enderecamento as e where a.idApontamento = e.idApontamento and a.status = 1 and e.status =1 and e.tipo = 0 and a.saldo >0;";
            //$sql = "select a.codigoPalete, a.tipo, a.saldo, e.idPosicao from apontamento as a, enderecamento as e where a.idApontamento = e.idApontamento and a.status = 1 and e.status =1  and a.saldo >0;";
            $sql = "select a.codigoPalete, e.idPosicao, a.paleteOrigem, a.saldo from apontamentoGenerico as a, enderecamento as e"
                    . " where a.idApontamentoGenerico = e.idApontamento and e.tipo =1 and a.status = 1 and e.status =1 ;";
            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function relatorioPaleteEnderecado() {
        try {

            //$sql = "select * from apontamento where  status = 1 and saldo > 0";
//na query abaixo ela so tras os paletes normais            
//$sql = "select a.codigoPalete, a.tipo, a.saldo, e.idPosicao from apontamento as a, enderecamento as e where a.idApontamento = e.idApontamento and a.status = 1 and e.status =1 and e.tipo = 0 and a.saldo >0;";
            //$sql = "select a.codigoPalete, a.tipo, a.saldo, e.idPosicao from apontamento as a, enderecamento as e where a.idApontamento = e.idApontamento and a.status = 1 and e.status =1  and a.saldo >0;";
            ///QUERY ABAIXO ATUALIDA PARA A NOVA, QUE MOSTRA O USUÁRIO RESPONSÁVEL PELO APONTAMENTO.
            //  $sql = "select a.codigoPalete, e.idPosicao, a.saldo, a.tipo from apontamento  as a left join enderecamento as e on a.idApontamento = e.idApontamento "
            //          . "left  join expedicao as ex on ex.codigoPalete = a.codigoPalete where ex.codigoPalete is null and e.status =1 and e.tipo =0;";
            //CÓDIGO FUNCINANDO PERFERITAMENTE ATÉ A DATA DE 20-07-2018, FOI ALTERADO POIS TINHA UMA ERRO E NÃO MOSTRAVA O RESPONSAVEL PELO NOVO REENDEREÇAMENTO
            //$sql = "select a.codigoPalete, e.idPosicao, a.saldo, a.tipo , hE.date, u.nome from apontamento  as a left join enderecamento as e on a.idApontamento = e.idApontamento left  join expedicao as ex on ex.codigoPalete = a.codigoPalete left join historicoEnderecamento as hE on hE.idEnderecamento = e.idEnderecamento left join usuario as u on u.idUsuario = hE.idUsuario where ex.codigoPalete is null and e.status =1 and e.tipo = 0;";
            $sql = 'select a.codigoPalete, e.idPosicao, a.saldo, a.tipo , hE.date, u.nome ,  
                     (select ua.nome from historicoReenderecamento as hRE, usuario as ua where ua.idUsuario = hRE.idUsuario and hRE.idEnderecamento = e.idEnderecamento order by date desc limit 1) as usuarioReenderecamento,
                                          (select hRE.date from historicoReenderecamento as hRE, usuario as ua where ua.idUsuario = hRE.idUsuario and hRE.idEnderecamento = e.idEnderecamento order by date desc limit 1) as dataReenderecamento 
                    from apontamento  as a 
                     left join enderecamento as e on a.idApontamento = e.idApontamento 
                     left  join expedicao as ex on ex.codigoPalete = a.codigoPalete 
                     left join historicoEnderecamento as hE on hE.idEnderecamento = e.idEnderecamento 
                     left join usuario as u on u.idUsuario = hE.idUsuario 
                     where ex.codigoPalete is null and e.status =1 and e.tipo = 0 or (ex.codigoPalete is not null and e.status =1 and e.tipo = 0)
                     GROUP BY a.codigoPalete ';
            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function relatorioExpedicao() {
        try {

            //$sql = "select * from apontamento where  status = 1 and saldo > 0";
            //    $sql = "select ordemExpedicao,codigoPalete from expedicao;";
            //    QUERY ANTIGA SEM USUARIO RESPONŚAVEL PELA EXPEDICAO 09-07-18
            //$sql = "select ex.ordemExpedicao, e.idPosicao, a.codigoPalete , a.tipo, a.saldo from apontamento as a, enderecamento as e, expedicao as ex "
            //       . " where a.idApontamento = e.idApontamento  and e.tipo = 0 and e.status = 0 and a.status = 0 and ex.codigoPalete = a.codigoPalete;";
            $sql = "select ex.ordemExpedicao, e.idPosicao, a.codigoPalete , a.tipo, a.saldo, hE.date ,u.nome from apontamento as a, enderecamento as e, expedicao as ex,  historicoExpedicao as hE, usuario as u "
                    . " where a.idApontamento = e.idApontamento  and e.tipo = 0 and e.status = 0 and a.status = 0 and ex.codigoPalete = a.codigoPalete and hE.idExpedicao = ex.idExpedicao and u.idUsuario = hE.idUsuario;";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function relatorioExpedicaoGenerico() {
        try {

            //$sql = "select * from apontamento where  status = 1 and saldo > 0";
            //    $sql = "select ordemExpedicao,codigoPalete from expedicao;";
            $sql = "select ex.ordemExpedicao, e.idPosicao, a.codigoPalete ,a.paleteOrigem, a.saldo from apontamentoGenerico as a, enderecamento as e, expedicao as ex "
                    . "where a.idApontamentoGenerico = e.idApontamento and e.tipo = 1 and e.status = 0 and a.status = 0 and ex.codigoPalete = a.codigoPalete;";
            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function todosApontamentos() {
        try {

            //$sql = "select * from apontamento where  status = 1 and saldo > 0";
            //$sql = "select codigoPalete, tipo,saldo from apontamento where status = 1 and saldo > 0; ";
            //$sql = "select * from apontamento";
            //ATUALIZAÇÃO DE CÓDIGO, O STATUS NÃO ESTAVA CORRESPONDETE
            //$sql = "select * from apontamento as a left join historicoApontamento as ha on ha.idApontamento = a.idApontamento  left join usuario as u on u.idUsuario = ha.idUsuario;";
            $sql = "select a.codigoPalete, a.tipo, a.saldo, a.status, u.nome, ha.data from apontamento as a left join historicoApontamento as ha on ha.idApontamento = a.idApontamento  left join usuario as u on u.idUsuario = ha.idUsuario where (a.status = 1 or a.status = 0 );";
            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function relatorioPaleteApontado() {
        try {

            //$sql = "select * from apontamento where  status = 1 and saldo > 0";
            //$sql = "select codigoPalete, tipo,saldo from apontamento where status = 1 and saldo > 0; ";
            $sql = "select a.codigoPalete , a.tipo, a.saldo from apontamento  as a left join enderecamento as e on a.idApontamento = e.idApontamento "
                    . "where a.status =1 and a.saldo >0 and e.idApontamento is null;";
            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function relatorioPaleteApontadoGenerico() {
        try {

            //$sql = "select * from apontamento where  status = 1 and saldo > 0";
            //$sql = "select codigoPalete, tipo,saldo from apontamento where status = 1 and saldo > 0; ";
            $sql = "select a.codigoPalete , a.paleteOrigem, a.saldo from apontamentoGenerico  as a left join enderecamento as e on a.idApontamentoGenerico = e.idApontamento"
                    . " where a.status =1 and a.saldo >0 and e.idApontamento is null  ;";
            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function verificaId($id) {
        try {

            $sql = "select * from apontamento where  status = 1 and idApontamento = :id ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function verificaIdGenerico($id) {
        try {

            $sql = "select * from apontamentoGenerico where  status = 1 and idApontamentoGenerico = :id ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function verificaCodigoReativar($codigo) {
        try {

            $sql = "select * from apontamento where  status = 0 and codigoPalete = :codigo ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function verificaCodigo($codigo) {
        try {

            //$sql = "select * from apontamento where  status = 1 and codigoPalete = :codigo ";
            //codígo comentado dia 03/07/18 para comparar em todos paletes não dependendo do status
            $sql = "select * from apontamento where   codigoPalete = :codigo ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function verificaCodigoComId($codigo, $id) {
        try {

            //$sql = "select * from apontamento where  status = 1 and codigoPalete = :codigo and idApontamento != :id";
            //codígo comentado dia 03/07/18 para comparar em todos paletes não dependendo do status
            $sql = "select * from apontamento where codigoPalete = :codigo and idApontamento != :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);
            $p_sql->bindValue(':id', $id);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

}
