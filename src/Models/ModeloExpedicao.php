<?php

namespace UBSValorem\Models;

use UBSValorem\Util\Conexao;
use PDO;
use UBSValorem\Entity\Expedicao;

class ModeloExpedicao {

    public function expedirGenerico(Expedicao $expedicao, $idUsuario) {
        try {
            $conexao = Conexao::getInstance();
            $sql = "insert into expedicao (ordemExpedicao, codigoPalete)"
                    . " values "
                    . " (:ordem,:palete)";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':ordem', $expedicao->getOrdemExpedicao());
            $p_sql->bindValue(':palete', $expedicao->getCodigoPalete());
            $p_sql->execute();
            $id = Conexao::getInstance()->lastInsertId();


            $status = "update apontamentoGenerico as a, enderecamento as e set e.status = 0, a.status =0"
                    . " where e.idApontamento = a.idApontamentoGenerico and a.codigoPalete = :palete and e.tipo = 1";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':palete', $expedicao->getCodigoPalete());
            $p_status->execute();

            $status = "update apontamentoGenerico as a set a.status = 0 "
                    . " where a.codigoPalete = :palete ";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':palete', $expedicao->getCodigoPalete());
            $p_status->execute();

            $status = "insert into historicoExpedicao(idExpedicao, idUsuario, date)"
                    . " values "
                    . "(:idExpedicao, :idUsuario,now())";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idExpedicao', $id);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->execute();


            $conexao->commit();
        } catch (Exception $ex) {
            
        }
    }

    public function finalizarOrdem($ordem, $peso,$idUsuario,$nota) {
        try {
            $conexao = Conexao::getInstance();

            $sql = "insert into ordem (codigoOrdem,peso,nota)"
                    . " value "
                    . " (:ordem,:peso,:nota)";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':ordem', $ordem);
            $p_sql->bindValue(':peso', $peso);
            $p_sql->bindValue(':nota', $nota);
            $p_sql->execute();
            $id = Conexao::getInstance()->lastInsertId();


            $status = "insert into historicoOrdem(idOrdem, idUsuario, data)"
                    . " values "
                    . "(:idOrdem, :idUsuario,now())";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idOrdem', $id);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->execute();

            $conexao->commit();

            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }

    public function expedir(Expedicao $expedicao, $idUsuario) {
        try {
            $conexao = Conexao::getInstance();
            $sql = "insert into expedicao (ordemExpedicao, codigoPalete)"
                    . " values"
                    . " (:ordem,:palete)";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':ordem', $expedicao->getOrdemExpedicao());
            $p_sql->bindValue(':palete', $expedicao->getCodigoPalete());
            $p_sql->execute();
            $id = Conexao::getInstance()->lastInsertId();


            $status = "update apontamento as a, enderecamento as e set e.status = 0, a.status =0"
                    . " where e.idApontamento = a.idApontamento and a.codigoPalete = :palete;";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':palete', $expedicao->getCodigoPalete());
            $p_status->execute();

            $status = "insert into historicoExpedicao(idExpedicao, idUsuario, date)"
                    . " values "
                    . "(:idExpedicao, :idUsuario,now())";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idExpedicao', $id);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->execute();


            $conexao->commit();
        } catch (Exception $ex) {
            
        }
    }

    public function verificaCodigo($codigo) {
        try {

            //$sql = "select * from expedicao where  ordemExpedicao = :codigo";
            $sql = "select * from ordem where  codigoOrdem = :codigo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

}
