<?php

namespace UBSValorem\Models;

use UBSValorem\Util\Conexao;
use PDO;
use UBSValorem\Entity\Enderecamento;

class ModeloEnderecamento {
    
    public function verificaDisponivelGenerico($codigo) {
        try {

            $sql = "select * from enderecamento as e, apontamentoGenerico as a  where e.status = 1 and a.status = 1 "
                    . " and a.idApontamentoGenerico = e.idApontamento and a.codigoPalete = :codigo and e.tipo = 1";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }
    
    public function enderecarGenerico(Enderecamento $enderecamento, $idUsuario) {
        try {

            $conexao = Conexao::getInstance();
            $sql = "insert into enderecamento (idApontamento, idPosicao, status, dataEnderecamento,tipo)"
                    . " values"
                    . " (:apontamento, :posicao, :status, now(), 1)";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':apontamento', $enderecamento->getIdApontamento());
            $p_sql->bindValue(':posicao', $enderecamento->getCodigoPosicao());
            $p_sql->bindValue(':status', $enderecamento->getStatus());
            $p_sql->execute();
            $id = Conexao::getInstance()->lastInsertId();

            $status = "insert into historicoEnderecamento (idUsuario, idEnderecamento, date,tipo)"
                    . " values "
                    . "(:idUsuario, :idEnderecamento, now(),1)";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idEnderecamento', $id);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->execute();

            $conexao->commit();
        } catch (Exception $ex) {
            
        }
    }
    
     public function verificaCodigoGenerico($codigo) {
        try {

            $sql = "select * from apontamentoGenerico as a, enderecamento as e where (a.status = 1 and a.codigoPalete = :codigo) and (e.idApontamento = a.idApontamentoGenerico);";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }
    
    public function verificaCodigoApontadoGenerico($codigo) {
        try {

            $sql = "select * from apontamentoGenerico as a where a.status = 1 and codigoPalete = :codigo;";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function editarGenerico($palete, $atual, $posicao, $idUsuario) {
        try {

            $conexao = Conexao::getInstance();
            $sql = "update enderecamento as e, apontamentoGenerico as a set e.idPosicao = :posicao, e.dataEnderecamento = now() where a.idApontamentoGenerico  = e.idApontamento and a.codigoPalete = :palete and e.tipo =1";
            $conexao->beginTransaction();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':posicao', $posicao);
            $p_sql->bindValue(':palete', $palete);
            $p_sql->execute();

            $sql = "select e.idEnderecamento from enderecamento as e, apontamentoGenerico as a where a.idApontamentoGenerico  = e.idApontamento and a.codigoPalete = :palete and e.tipo = 1";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':palete', $palete);
            $p_sql->execute();
            $id = $p_sql->fetch(PDO::FETCH_OBJ);




            $status = "insert into historicoReenderecamento (idUsuario, idEnderecamento, date, posicao)"
                    . " values "
                    . "(:idUsuario, :idEnderecamento, now(), :posicao)";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idEnderecamento', $id->idEnderecamento);
           // $p_status->bindValue(':idEnderecamento', 41);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->bindValue(':posicao', $atual);
            $p_status->execute();

            $conexao->commit();
        } catch (Exception $ex) {
            
        }
    }
    public function editar($palete, $atual, $posicao, $idUsuario) {
        try {

            $conexao = Conexao::getInstance();
            $sql = "update enderecamento as e, apontamento as a set e.idPosicao = :posicao, e.dataEnderecamento = now() where a.idApontamento = e.idApontamento and a.codigoPalete = :palete ";
            $conexao->beginTransaction();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':posicao', $posicao);
            $p_sql->bindValue(':palete', $palete);
            $p_sql->execute();

            $sql = "select e.idEnderecamento from enderecamento as e, apontamento as a where a.idApontamento = e.idApontamento and a.codigoPalete = :palete ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':palete', $palete);
            $p_sql->execute();
            $id = $p_sql->fetch(PDO::FETCH_OBJ);

            $status = "insert into historicoReenderecamento (idUsuario, idEnderecamento, date, posicao)"
                    . " values "
                    . "(:idUsuario, :idEnderecamento, now(), :posicao)";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idEnderecamento', $id->idEnderecamento);
           // $p_status->bindValue(':idEnderecamento', 41);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->bindValue(':posicao', $atual);
            $p_status->execute();

            $conexao->commit();
        } catch (Exception $ex) {
            
        }
    }

    public function verificaDisponivel($codigo) {
        try {

            $sql = "select * from enderecamento as e, apontamento as a  where e.status = 1 and a.status = 1 "
                    . " and a.idApontamento = e.idApontamento and a.codigoPalete = :codigo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function enderecar(Enderecamento $enderecamento, $idUsuario) {
        try {

            $conexao = Conexao::getInstance();
            $sql = "insert into enderecamento (idApontamento, idPosicao, status, dataEnderecamento,tipo)"
                    . " values"
                    . " (:apontamento, :posicao, :status, now(),0)";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':apontamento', $enderecamento->getIdApontamento());
            $p_sql->bindValue(':posicao', $enderecamento->getCodigoPosicao());
            $p_sql->bindValue(':status', $enderecamento->getStatus());
            $p_sql->execute();
            $id = Conexao::getInstance()->lastInsertId();

            $status = "insert into historicoEnderecamento (idUsuario, idEnderecamento, date,tipo)"
                    . " values "
                    . "(:idUsuario, :idEnderecamento, now(),0)";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idEnderecamento', $id);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->execute();

            $conexao->commit();
        } catch (Exception $ex) {
            
        }
    }

    public function verificaCodigoApontado($codigo) {
        try {

            $sql = "select * from apontamento as a where a.status = 1 and codigoPalete = :codigo;";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function verificaCodigo($codigo) {
        try {

            $sql = "select a.tipo, a.codigoPalete, a.saldo, e.idPosicao from apontamento as a, enderecamento as e where (a.status = 1 and codigoPalete = :codigo) and (e.idApontamento = a.idApontamento);";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

}
