<?php


namespace UBSValorem\Models;

use UBSValorem\Util\Conexao;
use PDO;

class ModeloSala {
    
    
     public function desativar($id) {
        try {
            $sql = "update  sala set status = 0 where idSala = :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }
     public function ativar($id) {
        try {
            $sql = "update  sala set status = 1 where idSala = :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }

    
     public function atualizar($sala,$setor,$id) {
        try {
            $sql = "update sala set descricao = :descricao , setor = :setor where idSala = :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':descricao', $sala);
            $p_sql->bindValue(':setor', $setor);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }

    
     public function cadastrar($sala,$setor) {
        try {
            $sql = "insert into sala (descricao,setor) values (:sala,:setor)";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':sala', $sala);
            $p_sql->bindValue(':setor', $setor);
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }

    
    public function verificaSalaEditar($sala,$setor,$id){
        try {
            $sql = "select * from sala where descricao = :descricao and setor = :setor and idSala != :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':descricao', $sala);
            $p_sql->bindValue(':setor', $setor);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }
    public function verificaSala($sala,$setor){
        try {
            $sql = "select * from sala where descricao = :descricao and setor = :setor";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':descricao', $sala);
            $p_sql->bindValue(':setor', $setor);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }
    public function sala($id){
        try {
            $sql = "select * from sala where idSala = :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

     public function salas() {
        try {
            $sql = "select * from sala ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }
    
}
