<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace UBSValorem\Models;

use UBSValorem\Util\Conexao;
use PDO;
use UBSValorem\Entity\OrdemRetorno;
use UBSValorem\Entity\Retorno;

class ModeloRetorno {

    public function relatorio() {
        try {

            $sql = "select o.codigoRetorno, e.idPosicao, a.codigoPalete , a.tipo, a.saldo, r.data from apontamento as a, enderecamento as e, retorno as r, ordemRetorno as o "
                    . "where a.idApontamento = e.idApontamento  and e.tipo = 0 and e.status = 0 and a.status = 0 and r.idApontamento = a.idApontamento and o.idOrdemRetorno = r.idOrdem;";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function finalizarOrdem($ordem, $quantidade, $idUsuario) {
        try {

            $sql = "update ordemRetorno set status  = 0, quantidade = :quantidade, idFinalizador = :id where codigoRetorno   = :ordem";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':ordem', $ordem);
            $p_sql->bindValue(':quantidade', $quantidade);
            $p_sql->bindValue(':id', $idUsuario);
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();



            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }

    public function expedir(Retorno $retorno) {
        try {
            $conexao = Conexao::getInstance();

            $sql = "insert into retorno (idUsuario, idOrdem, idApontamento,  data)"
                    . " values"
                    . " (:usuario, :ordem, :apontamento, now())";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':usuario', $retorno->getIdUsuario());
            $p_sql->bindValue(':ordem', $retorno->getIdOrdem());
            $p_sql->bindValue(':apontamento', $retorno->getIdApontamento());
            $p_sql->execute();

            $status = "update apontamento as a, enderecamento as e set e.status = 0, a.status =0"
                    . " where e.idApontamento = a.idApontamento and a.idApontamento = :id;";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':id', $retorno->getIdApontamento());
            $p_status->execute();

            $conexao->commit();
        } catch (Exception $ex) {
            
        }
    }

    public function verificaCodigoOrdemRetorno($codigo) {
        try {

            $sql = "select * from ordemRetorno where codigoRetorno = :codigo and status = 1 ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function editarOrdem(OrdemRetorno $ordem, $dados) {
        try {

            $conexao = Conexao::getInstance();
            $sql = "update ordemRetorno set codigoRetorno = :ordem, codigoLote = :lote where"
                    . " idOrdemRetorno = :id";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':ordem', $ordem->getCodigoRetorno());
            $p_sql->bindValue(':lote', $ordem->getCodigoLote());
            $p_sql->bindValue(':id', $ordem->getIdOrdemRetorno());
            $p_sql->execute();

            $status = "insert into historicoEdicaoOrdemRetorno (idUsuario, idOrdemRetorno, data, codigoOrdemRetorno, codigoLote)"
                    . " values "
                    . "(:idUsuario, :idOrdem, now(), :ordem, :lote)";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idUsuario', $ordem->getIdUsuario());
            $p_status->bindValue(':idOrdem', $ordem->getIdOrdemRetorno());
            $p_status->bindValue(':ordem', $dados->codigoRetorno);
            $p_status->bindValue(':lote', $dados->codigoLote);
            $p_status->execute();

            $conexao->commit();
        } catch (Exception $ex) {
            
        }
    }

    public function verificaOrdemRetornoEditar($codigo, $id) {
        try {

            $sql = "select * from ordemRetorno where codigoRetorno = :codigo and idOrdemRetorno != :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function ordens() {
        try {

            $sql = "select * from ordemRetorno where status = 1";
            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function cadastrarOrdemRetorno(OrdemRetorno $ordem) {
        try {

            $conexao = Conexao::getInstance();
            $sql = "insert into ordemRetorno (idUsuario, codigoRetorno, codigoLote, status, data)"
                    . " values"
                    . " (:usuario, :ordem, :lote, 1, now())";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':usuario', $ordem->getIdUsuario());
            $p_sql->bindValue(':ordem', $ordem->getCodigoRetorno());
            $p_sql->bindValue(':lote', $ordem->getCodigoLote());
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }

    public function ordem($id) {
        try {

            $sql = "select * from ordemRetorno where idOrdemRetorno = :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function verificaOrdemRetorno($codigo) {
        try {

            $sql = "select * from ordemRetorno where codigoRetorno = :codigo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

}
